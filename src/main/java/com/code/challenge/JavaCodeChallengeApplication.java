package com.code.challenge;

import com.code.challenge.entity.Reservation;
import com.code.challenge.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootApplication
public class JavaCodeChallengeApplication {

	@Autowired
	private ReservationService reservationService;

	public static void main(String[] args) {
		SpringApplication.run(JavaCodeChallengeApplication.class, args);
	}

	@Bean
	public CommandLineRunner run() {
		return (args) -> {
			System.out.println("Creating sample data");
			List<Reservation> reservations = createSampleReservations(10);
			System.out.println("Adding sample data to database");
			reservations.forEach(reservationService::addReservation);
			System.out.println("Finished loading sample data");
		};
	}

	private List<Reservation> createSampleReservations(int numReservations) {
		return IntStream.rangeClosed(1, numReservations).mapToObj(i -> new Reservation(i, "Reservation No. " + i, LocalDateTime.now())).collect(Collectors.toList());
	}

}
