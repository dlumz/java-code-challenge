package com.code.challenge.service;

import com.code.challenge.entity.Reservation;

import java.util.List;

public interface ReservationService {

    void addReservation(Reservation reservation);

    List<Reservation> getAllReservations();

}
